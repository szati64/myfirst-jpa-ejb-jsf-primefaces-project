package hu.schonherz.jee.client.api.service.user;

import hu.schonherz.jee.client.api.vo.UserVo;

public interface UserService {
	UserVo findByUsername(String username);
	UserVo registerUser(UserVo user);
}
