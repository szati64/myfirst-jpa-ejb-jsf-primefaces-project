package hu.schonherz.jee.client.api.service.user;

import hu.schonherz.jee.client.api.vo.RoleVo;

public interface RoleService {
	RoleVo findByName(String name);

	RoleVo save(RoleVo name);
}
