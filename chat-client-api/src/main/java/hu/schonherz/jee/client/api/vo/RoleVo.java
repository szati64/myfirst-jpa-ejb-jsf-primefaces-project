package hu.schonherz.jee.client.api.vo;

import java.io.Serializable;

public class RoleVo implements Serializable {
	private static final long serialVersionUID = 1779881620974702093L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
