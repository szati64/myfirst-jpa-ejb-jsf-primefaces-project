package hu.schonherz.jee.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import hu.schonherz.jee.client.api.service.user.UserServiceLocal;
import hu.schonherz.jee.client.api.service.user.UserServiceRemote;
import hu.schonherz.jee.client.api.vo.UserVo;
import hu.schonherz.jee.core.dao.RoleDao;
import hu.schonherz.jee.core.dao.UserDao;
import hu.schonherz.jee.core.entities.Role;
import hu.schonherz.jee.core.entities.User;
import hu.schonherz.jee.service.mapper.UserVoMapper;

@Stateless(mappedName = "UserService")
@Remote(UserServiceRemote.class)
@Local(UserServiceLocal.class)
@Interceptors(SpringBeanAutowiringInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserServiceBean implements UserServiceLocal, UserServiceRemote {
	private static final String ROLE_USER = "ROLE_USER";

	@Autowired
	UserDao userDao;
	
	@Autowired
	RoleDao roleDao;
	
	
	@Override
	public UserVo findByUsername(String username) {
		return UserVoMapper.toVo(userDao.findByUsername(username));
	}
	
	public UserVo registerUser(UserVo userVo) {
		User user = UserVoMapper.toEntity(userVo);
		
		List<Role> roles = new ArrayList<>();
		
		Role role = roleDao.findByName(ROLE_USER);
		roles.add(role);
		user.setRoles(roles);
		
		user = userDao.save(user);
		return UserVoMapper.toVo(user);
	}
	
	
}
