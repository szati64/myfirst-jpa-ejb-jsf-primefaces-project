package hu.schonherz.jee.service;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import hu.schonherz.jee.client.api.service.user.RoleServiceLocal;
import hu.schonherz.jee.client.api.service.user.RoleServiceRemote;
import hu.schonherz.jee.client.api.vo.RoleVo;
import hu.schonherz.jee.core.dao.RoleDao;
import hu.schonherz.jee.core.entities.Role;
import hu.schonherz.jee.service.mapper.RoleVoMapper;

@Stateless(mappedName = "RoleService")
@Remote(RoleServiceRemote.class)
@Local(RoleServiceLocal.class)
@Interceptors(SpringBeanAutowiringInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RoleServiceBean implements RoleServiceLocal, RoleServiceRemote {
	
	@Autowired
	RoleDao roleDao;
	
	@Override
	public RoleVo findByName(String name) {
		return RoleVoMapper.toVo(roleDao.findByName(name));
	}
	
	@Override
	public RoleVo save(RoleVo name) {
		Role role = roleDao.save(RoleVoMapper.toEntity(name));

		return RoleVoMapper.toVo(role);
	}
}
